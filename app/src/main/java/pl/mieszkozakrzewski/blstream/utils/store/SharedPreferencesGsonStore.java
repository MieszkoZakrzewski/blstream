package pl.mieszkozakrzewski.blstream.utils.store;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.lang.reflect.Type;

public class SharedPreferencesGsonStore<T> implements Store<T> {
    private final SharedPreferences sharedPreferences;
    private final Gson gson;
    private final Type clazz;
    private final String key;

    public SharedPreferencesGsonStore(SharedPreferences sharedPreferences, Gson gson, Type clazz, String key) {
        this.sharedPreferences = sharedPreferences;
        this.gson = gson;
        this.clazz = clazz;
        this.key = key;
    }

    @Override
    public T get() {
        String value = sharedPreferences.getString(key, null);
        if (value == null) {
            return null;
        }
        return gson.fromJson(value, clazz);
    }

    @Override
    public T get(T defaultValue) {
        T value = get();
        if (value == null) {
            return defaultValue;
        }
        return value;
    }

    @Override
    public void set(T data) {
        sharedPreferences.edit().putString(key, gson.toJson(data)).apply();
    }
}
