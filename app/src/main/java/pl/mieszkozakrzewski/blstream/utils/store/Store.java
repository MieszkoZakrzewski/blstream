package pl.mieszkozakrzewski.blstream.utils.store;

public interface Store<T> {
    T get();

    T get(T defaultValue);

    void set(T data);
}
