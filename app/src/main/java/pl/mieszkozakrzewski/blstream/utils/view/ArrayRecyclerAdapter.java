package pl.mieszkozakrzewski.blstream.utils.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public abstract class ArrayRecyclerAdapter<D, T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {
    private List<D> data;
    private List<D> filteredData;
    private final LayoutInflater inflater;

    protected ArrayRecyclerAdapter(Context context, List<D> data) {
        this.data = new ArrayList<>(data);
        this.filteredData = new ArrayList<>(data);
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public final T onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return onCreateViewHolder(inflater, viewGroup, viewType);
    }

    @Override
    public final void onBindViewHolder(T viewHolder, int index) {
        onBindViewHolder(getItem(index), viewHolder, index);
    }

    @Override
    public int getItemCount() {
        return filteredData.size();
    }

    protected abstract T onCreateViewHolder(LayoutInflater inflater, ViewGroup viewGroup, int viewType);

    protected abstract void onBindViewHolder(D data, T viewHolder, int index);

    protected D getItem(int index) {
        if (index < 0 || index >= filteredData.size()) {
            return null;
        }
        return filteredData.get(index);
    }

    public void swapData(List<D> data) {
        this.filteredData = new ArrayList<>(data);
        notifyDataSetChanged();
    }

    public void notifyItemChanged(D item) {
        int location = data.indexOf(item);
        int fLocation = filteredData.indexOf(item);
        if (location == fLocation) {
            data.add(location, item);
            data.remove(location + 1);
            filteredData.add(location, item);
            filteredData.remove(location + 1);
        } else {
            data.add(location, item);
            data.remove(location + 1);
            filteredData.add(fLocation, item);
            filteredData.remove(fLocation + 1);
        }
        notifyItemChanged(fLocation);
    }

    public List<D> getItems() {
        return data;
    }

    public void addItem(D item) {
        data.add(item);
        filteredData.add(item);
        notifyItemInserted(data.size() - 1);
    }

    public void removeItem(int location) {
        if (data.size() == filteredData.size()) {
            data.remove(location);
        } else {
            data.remove(filteredData.get(location));
        }
        filteredData.remove(location);
        notifyItemRemoved(location);
    }
}

