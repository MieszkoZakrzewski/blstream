package pl.mieszkozakrzewski.blstream.utils.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ViewAnimator;

@SuppressWarnings("ConstantConditions")
public class BetterViewAnimator extends ViewAnimator {

    @SuppressWarnings("WeakerAccess")
    public BetterViewAnimator(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @SuppressWarnings("WeakerAccess")
    public int getDisplayedChildId() {
        return getChildAt(getDisplayedChild()).getId();
    }

    public void setDisplayedChildId(int id) {
        if (getDisplayedChildId() == id) {
            return;
        }
        for (int i = 0, count = getChildCount(); i < count; i++) {
            if (getChildAt(i).getId() == id) {
                setDisplayedChild(i);
                return;
            }
        }
        throw new IllegalArgumentException("No view with ID " + id);
    }
}
