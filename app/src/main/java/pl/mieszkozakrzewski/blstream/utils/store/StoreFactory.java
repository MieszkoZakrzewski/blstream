package pl.mieszkozakrzewski.blstream.utils.store;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.lang.reflect.Type;

public class StoreFactory {
    private final SharedPreferences sharedPreferences;
    private final Gson gson;

    public StoreFactory(Context context) {
        this.sharedPreferences = SharedPreferencesManager.get(context);
        this.gson = new Gson();
    }

    public <T> Store<T> createStore(String key, Type type) {
        return new SharedPreferencesGsonStore<>(sharedPreferences, gson, type, key);
    }
}
