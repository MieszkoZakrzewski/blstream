package pl.mieszkozakrzewski.blstream.utils.store;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesManager {

    private static final String SHARED_PREFERENCES_NAME = "pl.mieszkozakrzewski.blstream";

    public static SharedPreferences get(Context context) {
        return context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }
}
