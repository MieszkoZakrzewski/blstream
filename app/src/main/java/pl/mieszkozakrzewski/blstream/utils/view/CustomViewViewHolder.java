package pl.mieszkozakrzewski.blstream.utils.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import pl.mieszkozakrzewski.blstream.R;
import pl.mieszkozakrzewski.blstream.ui.view.listener.ItemTouchHelperViewListener;

public class CustomViewViewHolder<T extends View> extends RecyclerView.ViewHolder implements ItemTouchHelperViewListener {
    private final T customView;

    public CustomViewViewHolder(T itemView) {
        super(itemView);
        customView = itemView;
    }

    public T getCustomView() {
        return customView;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onItemSelected() {
        customView.setBackgroundColor(customView.getContext().getResources().getColor(R.color.colorAccentLight));
    }

    @Override
    public void onItemClear() {
        customView.setBackgroundColor(0);
    }
}

