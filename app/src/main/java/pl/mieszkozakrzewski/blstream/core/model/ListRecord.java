package pl.mieszkozakrzewski.blstream.core.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ListRecord implements Parcelable {
    private int id;
    private String text;
    private boolean isSelected;

    public ListRecord(int id, String text, boolean isSelected) {
        this.id = id;
        this.text = text;
        this.isSelected = isSelected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ListRecord && id == ((ListRecord) o).id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.text);
        dest.writeByte(isSelected ? (byte) 1 : (byte) 0);
    }

    protected ListRecord(Parcel in) {
        this.id = in.readInt();
        this.text = in.readString();
        this.isSelected = in.readByte() != 0;
    }

    public static final Creator<ListRecord> CREATOR = new Creator<ListRecord>() {
        public ListRecord createFromParcel(Parcel source) {
            return new ListRecord(source);
        }

        public ListRecord[] newArray(int size) {
            return new ListRecord[size];
        }
    };
}