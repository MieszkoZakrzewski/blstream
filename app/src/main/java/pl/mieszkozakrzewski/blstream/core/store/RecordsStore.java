package pl.mieszkozakrzewski.blstream.core.store;

import android.content.Context;

import java.util.List;

import pl.mieszkozakrzewski.blstream.core.model.ListRecord;
import pl.mieszkozakrzewski.blstream.utils.store.Store;
import pl.mieszkozakrzewski.blstream.utils.store.StoreFactory;

public class RecordsStore {
    private final Store<RecordsStoreModel> recordsStore;

    public RecordsStore(Context context) {
        recordsStore = new StoreFactory(context).createStore("records_store", RecordsStoreModel.class);
    }

    public void put(List<ListRecord> records) {
        recordsStore.set(new RecordsStoreModel(records));
    }

    public List<ListRecord> get() {
        RecordsStoreModel model = recordsStore.get();
        return model != null && model.getRecords() != null ? moderateIds(model) : RecordsStoreModel.getEmptyRecords();
    }

    private List<ListRecord> moderateIds(RecordsStoreModel model) {
        List<ListRecord> records = model.getRecords();
        for (int i = 0; i < records.size(); i++) {
            records.get(i).setId(i);
        }

        return records;
    }
}
