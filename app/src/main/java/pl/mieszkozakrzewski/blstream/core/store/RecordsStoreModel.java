package pl.mieszkozakrzewski.blstream.core.store;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import pl.mieszkozakrzewski.blstream.core.model.ListRecord;

public class RecordsStoreModel implements Parcelable {
    private List<ListRecord> records;

    public RecordsStoreModel(List<ListRecord> records) {
        this.records = records;
    }

    public List<ListRecord> getRecords() {
        return records;
    }

    public void setRecords(List<ListRecord> records) {
        this.records = records;
    }

    public static List<ListRecord> getEmptyRecords() {
        return new ArrayList<>();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(records);
    }

    protected RecordsStoreModel(Parcel in) {
        this.records = in.createTypedArrayList(ListRecord.CREATOR);
    }

    public static final Parcelable.Creator<RecordsStoreModel> CREATOR = new Parcelable.Creator<RecordsStoreModel>() {
        public RecordsStoreModel createFromParcel(Parcel source) {
            return new RecordsStoreModel(source);
        }

        public RecordsStoreModel[] newArray(int size) {
            return new RecordsStoreModel[size];
        }
    };
}
