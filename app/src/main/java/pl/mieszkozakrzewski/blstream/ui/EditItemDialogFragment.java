package pl.mieszkozakrzewski.blstream.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import pl.mieszkozakrzewski.blstream.R;
import pl.mieszkozakrzewski.blstream.core.model.ListRecord;
import pl.mieszkozakrzewski.blstream.ui.view.listener.OnItemSavedListener;

public class EditItemDialogFragment extends DialogFragment {

    public static final String TAG_EDIT_ITEM_FRAGMENT = "TAG_EDIT_ITEM_FRAGMENT";
    private static final String ARG_RECORD = "ARG_RECORD";

    private OnItemSavedListener onItemSavedListener;

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final ListRecord record = getArguments().getParcelable(ARG_RECORD);
        View layout = LayoutInflater.from(getActivity()).inflate(R.layout.view_edit_item, null);
        final EditText editText = (EditText) layout.findViewById(R.id.view_edit_item_text);
        editText.setText(record != null ? record.getText() : "");
        editText.setSelection(editText.getText().length());
        return new AlertDialog.Builder(getActivity())
                .setView(layout)
                .setPositiveButton(getString(R.string.save), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (onItemSavedListener != null && record != null) {
                            record.setText(editText.getText().toString());
                            onItemSavedListener.onSave(record);
                        }
                    }
                })
                .setNegativeButton(getString(R.string.cancel), null).show();
    }

    public void setOnItemSavedListener(OnItemSavedListener onItemSavedListener) {
        this.onItemSavedListener = onItemSavedListener;
    }

    public static void show(FragmentManager fragmentManager, ListRecord record, OnItemSavedListener onItemSavedListener) {
        create(record, onItemSavedListener).show(fragmentManager, TAG_EDIT_ITEM_FRAGMENT);
    }

    private static EditItemDialogFragment create(ListRecord record, OnItemSavedListener onItemSavedListener) {
        EditItemDialogFragment fragment = new EditItemDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_RECORD, record);
        fragment.setArguments(args);
        fragment.setOnItemSavedListener(onItemSavedListener);
        return fragment;
    }
}
