package pl.mieszkozakrzewski.blstream.ui.view.listener;

public interface OnItemSwipedListener {
    void onSwiped(int position);
}
