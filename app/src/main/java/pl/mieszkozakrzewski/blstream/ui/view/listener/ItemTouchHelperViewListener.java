package pl.mieszkozakrzewski.blstream.ui.view.listener;

public interface ItemTouchHelperViewListener {

    void onItemSelected();

    void onItemClear();
}
