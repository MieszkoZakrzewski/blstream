package pl.mieszkozakrzewski.blstream.ui.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import pl.mieszkozakrzewski.blstream.R;
import pl.mieszkozakrzewski.blstream.core.model.ListRecord;
import pl.mieszkozakrzewski.blstream.ui.view.listener.OnItemClickListener;
import pl.mieszkozakrzewski.blstream.utils.view.ArrayRecyclerAdapter;
import pl.mieszkozakrzewski.blstream.utils.view.CustomViewViewHolder;

public class ListRecyclerViewAdapter extends ArrayRecyclerAdapter<ListRecord, CustomViewViewHolder<ListItemView>> {

    private final OnItemClickListener onItemClickListener;

    public ListRecyclerViewAdapter(Context context, List<ListRecord> data, OnItemClickListener onItemClickListener) {
        super(context, data);
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    protected CustomViewViewHolder<ListItemView> onCreateViewHolder(LayoutInflater inflater, ViewGroup viewGroup, int viewType) {
        return new CustomViewViewHolder<>((ListItemView) inflater.inflate(R.layout.view_list_item, viewGroup, false));
    }

    @Override
    protected void onBindViewHolder(ListRecord data, final CustomViewViewHolder<ListItemView> viewHolder, int index) {
        if (data == null) return;
        viewHolder.getCustomView().setData(data);
        viewHolder.getCustomView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(getItem(viewHolder.getAdapterPosition()));
            }
        });
    }

    public List<ListRecord> filter(String query) {
        query = query.toLowerCase();

        if (!query.isEmpty()) {
            final List<ListRecord> filteredModelList = new ArrayList<>();
            for (ListRecord model : getItems()) {
                final String text = model.getText().toLowerCase();
                if (text.contains(query)) {
                    filteredModelList.add(model);
                }
            }
            return filteredModelList;
        } else {
            return getItems();
        }
    }

    public int getNextId() {
        return getItemCount() > 0 ? getItems().get(getItemCount() - 1).getId() + 1 : 0;
    }
}
