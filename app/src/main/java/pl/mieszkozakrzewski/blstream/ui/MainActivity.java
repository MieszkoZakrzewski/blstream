package pl.mieszkozakrzewski.blstream.ui;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import butterknife.Bind;
import pl.mieszkozakrzewski.blstream.R;
import pl.mieszkozakrzewski.blstream.core.model.ListRecord;
import pl.mieszkozakrzewski.blstream.core.store.RecordsStore;
import pl.mieszkozakrzewski.blstream.core.store.RecordsStoreModel;
import pl.mieszkozakrzewski.blstream.ui.base.BaseActivity;
import pl.mieszkozakrzewski.blstream.ui.view.ListRecyclerViewAdapter;
import pl.mieszkozakrzewski.blstream.ui.view.listener.OnItemClickListener;
import pl.mieszkozakrzewski.blstream.ui.view.listener.OnItemSavedListener;
import pl.mieszkozakrzewski.blstream.ui.view.listener.OnItemSwipedListener;
import pl.mieszkozakrzewski.blstream.utils.view.BetterViewAnimator;
import pl.mieszkozakrzewski.blstream.utils.view.ItemTouchHelperCallback;

public class MainActivity extends BaseActivity implements OnItemClickListener, OnItemSavedListener,
        OnItemSwipedListener, SearchView.OnQueryTextListener {

    private static final String TAG_RECORDS_STORE = "TAG_RECORDS_STORE";
    private static final String TAG_SEARCH_QUERY = "TAG_SEARCH_QUERY";

    @Bind(R.id.main_activity_toolbar)
    Toolbar toolbar;

    @Bind(R.id.main_activity_floating_button)
    FloatingActionButton floatingActionButton;

    @Bind(R.id.content_main_view_animator)
    BetterViewAnimator viewAnimator;

    @Bind(R.id.content_main_list)
    RecyclerView recyclerView;

    private ListRecyclerViewAdapter adapter;
    private RecordsStore recordsStore;
    private SearchView searchView;
    private String query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        recordsStore = new RecordsStore(this);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (adapter != null) {
                    ListRecord record = new ListRecord(adapter.getNextId(), getString(R.string.new_item), false);
                    adapter.addItem(record);
                    evaluateEmptyView();
                    EditItemDialogFragment.show(getSupportFragmentManager(), record, MainActivity.this);
                }
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(false);
        adapter = new ListRecyclerViewAdapter(this, getData(savedInstanceState), this);
        recyclerView.setAdapter(adapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelperCallback(this));
        itemTouchHelper.attachToRecyclerView(recyclerView);
        evaluateEmptyView();

        if (savedInstanceState != null) {
            restoreDialogFragment();
            query = savedInstanceState.getString(TAG_SEARCH_QUERY);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(TAG_RECORDS_STORE, new RecordsStoreModel(adapter.getItems()));
        if (searchView != null) {
            outState.putString(TAG_SEARCH_QUERY, searchView.getQuery().toString());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (adapter != null) {
            recordsStore.put(adapter.getItems());
        }
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    public void onItemClick(ListRecord record) {
        EditItemDialogFragment.show(getSupportFragmentManager(), record, this);
    }

    @Override
    public void onSave(final ListRecord record) {
        if (adapter != null) {
            adapter.notifyItemChanged(record);
            if (searchView != null) {
                adapter.swapData(adapter.filter(searchView.getQuery().toString()));
            }
        }
    }

    @Override
    public void onSwiped(int position) {
        if (adapter != null && viewAnimator != null) {
            adapter.removeItem(position);
            evaluateEmptyView();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
        if (query != null) {
            searchView.setIconified(false);
            searchView.setQuery(query, true);
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        if (adapter != null && recyclerView != null) {
            adapter.swapData(adapter.filter(query));
            recyclerView.scrollToPosition(0);
        }
        evaluateEmptyView();
        floatingActionButton.setVisibility(query.isEmpty() ? View.VISIBLE : View.GONE);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private List<ListRecord> getData(Bundle savedInstanceState) {
        return savedInstanceState != null && savedInstanceState.getParcelable(TAG_RECORDS_STORE) != null ?
                ((RecordsStoreModel) savedInstanceState.getParcelable(TAG_RECORDS_STORE)).getRecords() : recordsStore.get();
    }

    private void restoreDialogFragment() {
        EditItemDialogFragment dialogFragment = (EditItemDialogFragment) getSupportFragmentManager()
                .findFragmentByTag(EditItemDialogFragment.TAG_EDIT_ITEM_FRAGMENT);
        if (dialogFragment != null) {
            dialogFragment.setOnItemSavedListener(this);
        }
    }

    private void evaluateEmptyView() {
        if (adapter != null && viewAnimator != null) {
            if (adapter.getItemCount() == 0) {
                viewAnimator.setDisplayedChildId(R.id.content_main_empty_list);
            } else {
                viewAnimator.setDisplayedChildId(R.id.content_main_list);
            }
        }
    }
}
