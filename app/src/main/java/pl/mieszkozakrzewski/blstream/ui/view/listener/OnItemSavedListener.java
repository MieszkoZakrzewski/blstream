package pl.mieszkozakrzewski.blstream.ui.view.listener;

import pl.mieszkozakrzewski.blstream.core.model.ListRecord;

public interface OnItemSavedListener {
    void onSave(ListRecord record);
}
