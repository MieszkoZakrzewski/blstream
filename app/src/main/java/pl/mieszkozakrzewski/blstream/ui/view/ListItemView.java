package pl.mieszkozakrzewski.blstream.ui.view;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.mieszkozakrzewski.blstream.R;
import pl.mieszkozakrzewski.blstream.core.model.ListRecord;

public class ListItemView extends LinearLayout {

    @Bind(R.id.view_list_item_text)
    TextView text;

    @Bind(R.id.view_list_item_check_box)
    AppCompatCheckBox checkBox;

    private ListRecord data;

    public ListItemView(Context context) {
        super(context);
    }

    public ListItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ListItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setData(ListRecord data) {
        this.data = data;
        prepareView();
    }

    private void prepareView() {
        text.setText(data.getText());
        checkBox.setChecked(data.isSelected());
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                data.setIsSelected(isChecked);
            }
        });
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }
}
